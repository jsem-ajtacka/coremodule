<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Services;

use JaAdmin\CoreModule\Models\EntityManagerDecorator;
use JaAdmin\CoreModule\Models\Extension;
use JaAdmin\CoreModule\Utils\KW;
use Doctrine\Persistence\ObjectRepository;

class ExtensionService
{
    private ObjectRepository $extensionRepository;

    public function __construct(private EntityManagerDecorator $em)
    {
        $this->extensionRepository = $this->em->getRepository(Extension::class);
    }

    public function getAll(): array
    {
        return $this->extensionRepository->findAll();
    }

    public function getActiveExtensionsCount(): int
    {
        return $this->extensionRepository->count([KW::Active => true]);
    }

    public function getItem(string $key): Extension|null
    {
        return $this->extensionRepository->findOneBy([KW::Key => $key]);
    }
}
