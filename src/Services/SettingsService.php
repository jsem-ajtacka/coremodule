<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Services;

class SettingsService
{
    public function __construct(public array $settings) {}
}
