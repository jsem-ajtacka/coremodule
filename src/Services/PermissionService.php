<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Services;

use Doctrine\Persistence\ObjectRepository;
use JaAdmin\CoreModule\Models\EntityManagerDecorator;
use JaAdmin\CoreModule\Models\UserRole;
use JaAdmin\CoreModule\Models\Extension;
use JaAdmin\CoreModule\Models\Permission;

class PermissionService
{
    private ObjectRepository $userRoleRepository;
    private ObjectRepository $extensionRepository;
    private ObjectRepository $permissionRepository;

    public function __construct(private EntityManagerDecorator $em) {
        $this->userRoleRepository = $this->em->getRepository(UserRole::class);
        $this->extensionRepository = $this->em->getRepository(Extension::class);
        $this->permissionRepository = $this->em->getRepository(Permission::class);
    }

    public function create(): \Nette\Security\Permission
    {
        $acl = new \Nette\Security\Permission;

        $userRoles = $this->userRoleRepository->findAll();
        foreach ($userRoles as $role) {
            $acl->addRole($role->getTitle());
        }

        $extensions = $this->extensionRepository->findAll();
        foreach ($extensions as $extension) {
            $acl->addResource($extension->getKey());
        }

        $permissions = $this->permissionRepository->findAll();
        foreach ($permissions as $permission) {
            $acl->allow($permission->getUserRole()->getTitle(), $permission->getExtension()->getKey(), $permission->getPrivilege());
        }

        return $acl;
    }
}
