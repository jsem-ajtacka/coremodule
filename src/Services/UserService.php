<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Services;

use JaAdmin\CoreModule\Models\EntityManagerDecorator;
use JaAdmin\CoreModule\Models\PasswordResetRequest;
use JaAdmin\CoreModule\Models\User;
use JaAdmin\CoreModule\Utils\KW;
use DateTime;
use Doctrine\Persistence\ObjectRepository;
use Nette\Security\Passwords;

class UserService
{
    private ObjectRepository $userRepository;

    private ObjectRepository $passwordResetRequestRepository;

    public function __construct(private EntityManagerDecorator $em, private Passwords $passwords)
    {
        $this->userRepository = $this->em->getRepository(User::class);
        $this->passwordResetRequestRepository = $this->em->getRepository(PasswordResetRequest::class);
    }

    public function isExistingUser(string $email): bool
    {
        return !is_null($this->getUserByEmail($email));
    }

    public function getUserByEmail(string $email): User|null
    {
        return $this->userRepository->findOneBy([KW::Email => $email]);
    }

    public function changePassword(string $hash, string $password): void
    {
        if ($this->isPasswordResetRequestValid($hash)) {
            $request = $this->passwordResetRequestRepository->findOneBy([KW::Hash => $hash]);
            $user = $request->getUser();
            $user->setPassword($this->passwords->hash($password));
            $this->em->persist($user);
            $this->em->remove($request);
            $this->em->flush();
        }
    }

    public function savePasswordResetRequest(string $email): PasswordResetRequest
    {
        $generatedAt = new DateTime();
        $hash = $this->passwords->hash($generatedAt->getTimestamp() . $email);
        $user = $this->getUserByEmail($email);

        $passwordResetRequest = new PasswordResetRequest();
        $passwordResetRequest->setGeneratedAt($generatedAt)
            ->setHash($hash)
            ->setUser($user);

        $this->em->persist($passwordResetRequest);
        $this->em->flush();

        return $passwordResetRequest;
    }

    public function isPasswordResetRequestValid($hash): bool
    {
        $request = $this->passwordResetRequestRepository->findOneBy([KW::Hash => $hash]);
        return !is_null($request) && !$request->isOutdated() && ($request->getHash() === $hash);
    }
}
