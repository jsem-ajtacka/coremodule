<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\DI;

use JaAdmin\CoreModule\Services\SettingsService;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

final class CoreModuleExtension extends CompilerExtension {
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'settings' => Expect::array()
        ]);
    }

    public function loadConfiguration(): void
    {
        $config = $this->config;

        $this->getContainerBuilder()
            ->addDefinition('settings')
            ->setFactory(SettingsService::class)
            ->setArguments([$config->settings]);
    }
}
