<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Authentication;

use JaAdmin\CoreModule\Models\User;
use Nette\Security\SimpleIdentity;

class UserIdentity extends SimpleIdentity
{
	private string $username;
    private string $firstname;
    private string $lastname;
    private string $email;

	public function __construct(User $user)
	{
        parent::__construct($user->getId(), $user->getRoles());

		$this->id = $user->getId();
		$this->roles = $user->getRoles();
		$this->username = $user->getFirstname() . " " . $user->getLastname();
		$this->firstname = $user->getFirstname();
        $this->lastname = $user->getLastname();
		$this->email = $user->getEmail();
	}

	public function getUsername(): string
	{
		return $this->username;
	}

	public function setUsername(string $username): UserIdentity
	{
		$this->username = $username;
		return $this;
	}

	public function getFirstname(): string
	{
		return $this->firstname;
	}

	public function setFirstname(string $firstname): UserIdentity
	{
		$this->firstname = $firstname;
		return $this;
	}

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): UserIdentity
    {
        $this->lastname = $lastname;
        return $this;
    }

    public function getEmail(): string
	{
		return $this->email;
	}

	public function setEmail(string $email) : UserIdentity
	{
		$this->email = $email;
		return $this;
	}
}
