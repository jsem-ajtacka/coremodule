<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Authentication;

use JaAdmin\CoreModule\Services\UserService;
use Nette\Security\AuthenticationException;
use Nette\Security\Authenticator;
use Nette\Security\Passwords;

class LoginFormAuthenticator implements Authenticator
{
    private const InvalidCredentials = "Login failed due to invalid credentials.";

    public function __construct(private UserService $userService, private Passwords $passwords) {}

    /**
     * @throws AuthenticationException
     */
    public function authenticate(string $email, string $password): UserIdentity
    {
        $user = $this->userService->getUserByEmail($email);

        if (empty($user)) {
            throw new AuthenticationException(self::InvalidCredentials, self::IDENTITY_NOT_FOUND);

        } else if (!$this->passwords->verify($password, $user->getPassword())) {
            throw new AuthenticationException(self::InvalidCredentials, self::INVALID_CREDENTIAL);

        } else {
            return new UserIdentity($user);
        }
    }
}
