<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Presenters;

use JaAdmin\CoreModule\Services\ExtensionService;
use JaAdmin\CoreModule\Services\SettingsService;
use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use JetBrains\PhpStorm\NoReturn;
use Nette\Application\Attributes\Persistent;
use Nette\Application\UI\Presenter;
use Nette\DI\Attributes\Inject;
use Nette\Localization\Translator;

class SecurePresenter extends Presenter
{
    private const LogoutSuccess = "coreModule.login.flashMessage.logoutSuccess";
    private const RedirectLink = ":Core:Login:default";

    #[Persistent]
    public string $locale;

    #[Inject]
    public ExtensionService $extensionService;

    #[Inject]
    public SettingsService $settingsService;

    #[Inject]
    public Translator $translator;

    protected string $websiteUrl;
    protected array $extensions;

    public function startup()
    {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect(self::RedirectLink);
        }

        $this->template->locale = $this->locale;

        $this->websiteUrl = $this->settingsService->settings['website']['url'];
        $this->extensions = $this->extensionService->getAll();

        $this->template->websiteUrl = $this->websiteUrl;
        $this->template->extensions = $this->extensions;
        $this->template->activeExtensionsCount = $this->extensionService->getActiveExtensionsCount();
    }

    #[NoReturn]
    public function handleLogout()
    {
        $this->getUser()->logout(true);
        $this->flashMessage(new FlashMessage(self::LogoutSuccess, FlashMessageType::Success));
        $this->redirect(self::RedirectLink);
    }

    public function formatLayoutTemplateFiles(): array
    {
        $layouts = parent::formatLayoutTemplateFiles();
        $layouts[] = __DIR__ . "/templates/@layout.latte";
        return $layouts;
    }
}
