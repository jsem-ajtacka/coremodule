<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Presenters;

use Nette\Application\BadRequestException;
use Nette\Application\Request;
use Nette\Application\UI\Presenter;

final class Error4xxPresenter extends Presenter
{
	public function startup(): void
	{
		parent::startup();
		if (!$this->getRequest()->isMethod(Request::FORWARD)) {
			$this->error();
		}
	}

	public function renderDefault(BadRequestException $exception): void
	{
		$this->template->errorCode = $exception->getCode();
		$this->template->setFile(__DIR__ . '/templates/Error/4xx.latte');
	}
}
