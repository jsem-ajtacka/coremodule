<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Presenters;

use JaAdmin\CoreModule\Services\SettingsService;
use JaAdmin\CoreModule\Services\UserService;
use JaAdmin\CoreModule\Utils\Email;
use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use Exception;
use Nette\Application\UI\Form;
use Nette\DI\Attributes\Inject;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Tracy\ILogger;

final class PasswordResetRequestPresenter extends UnsecurePresenter
{
    private const EmailSubject = "coreModule.passwordResetRequest.email.subject";
    private const RequestSuccess = "coreModule.passwordResetRequest.flashMessage.requestSuccess";
    private const RequestFail = "coreModule.passwordResetRequest.flashMessage.requestFail";
    private const RequestError = 'Requested password reset by non-existing user: %s';
    private const RedirectLink = ":Core:Login:default";

    #[Inject]
    public UserService $userService;

    #[Inject]
    public Email $email;

    public string $emailFrom;

    public function __construct(private SettingsService $settingsService)
    {
        parent::__construct();

        $this->emailFrom = $this->settingsService->settings['email']['from'];
    }

    public function startup()
    {
        parent::startup();
    }

    public function createComponentPasswordResetRequestForm(): Form
    {
        $form = new Form();

        $form->addEmail("email", "coreModule.passwordResetRequest.form.email.label")
            ->setHtmlAttribute("placeholder", "coreModule.passwordResetRequest.form.email.placeholder")
            ->setRequired();

        $form->addSubmit("submit", "coreModule.passwordResetRequest.form.submit.label");

        $form->onSuccess[] = [$this, "onPasswordResetRequestFormSuccess"];

        return $form;
    }

    public function onPasswordResetRequestFormSuccess(Form $form, ArrayHash $values): void
    {
        $flashMessage = new FlashMessage(self::RequestSuccess, FlashMessageType::Success);

        if ($this->userService->isExistingUser($values->email)) {
            $passwordResetRequest = $this->userService->savePasswordResetRequest($values->email);

            try {
                $this->email->setFrom($this->emailFrom)
                    ->setTo($values->email)
                    ->setSubject(self::EmailSubject)
                    ->setParams([
                        "subject" => self::EmailSubject,
                        "link" => $this->link("//:Core:PasswordReset:default", ["hash" => $passwordResetRequest->getHash()])
                    ])
                    ->setLatteTemplate(__DIR__ . "/templates/PasswordResetRequest/email." . $this->locale . ".latte");

                $this->email->send();
            } catch (Exception $e) {
                Debugger::log($e->getMessage(), ILogger::INFO);
                $flashMessage = new FlashMessage(self::RequestFail, FlashMessageType::Danger);
            }
        } else {
            Debugger::log(printf(self::RequestError, $values->email));
        }

        $this->flashMessage($flashMessage);
        $this->redirect(self::RedirectLink);
    }
}
