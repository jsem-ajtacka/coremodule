<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Presenters;

use Nette\Application\Attributes\Persistent;
use Nette\Application\UI\Presenter;
use Nette\DI\Attributes\Inject;
use Nette\Localization\Translator;

class UnsecurePresenter extends Presenter
{
    private const RedirectLink = ":Core:Overview:default";

    #[Persistent]
    public string $locale;

    #[Inject]
    public Translator $translator;

    public function startup()
    {
        parent::startup();

        if ($this->getUser()->isLoggedIn()) {
            $this->redirect(self::RedirectLink);
        }

        $this->template->locale = $this->locale;
    }
}
