<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Presenters;

use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use Nette\Application\UI\Form;
use Nette\DI\Attributes\Inject;
use Nette\Security\AuthenticationException;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Tracy\ILogger;

final class LoginPresenter extends UnsecurePresenter
{
    private const LoginSuccess = "coreModule.login.flashMessage.loginSuccess";
    private const LoginFail = "coreModule.login.flashMessage.loginFail";
    private const RedirectLink = ":Core:Overview:default";

    #[Inject]
    public ILogger $logger;

    public function startup()
    {
        parent::startup();
    }

    public function createComponentLoginForm(): Form
    {
        $form = new Form();
        
        $form->setTranslator($this->translator);

        $form->addEmail("email", "coreModule.login.form.email.label")
            ->setHtmlAttribute("placeholder", "coreModule.login.form.email.placeholder")
            ->setRequired();

        $form->addPassword("password", "coreModule.login.form.password.label")
            ->setHtmlAttribute("placeholder", "coreModule.login.form.password.placeholder")
            ->setRequired();

        $form->addSubmit("submit", "coreModule.login.form.submit.label");

        $form->onSuccess[] = [$this, "onSuccess"];

        return $form;
    }

    public function onSuccess(Form $form, ArrayHash $values): void
    {
        $flashMessage = new FlashMessage(self::LoginSuccess, FlashMessageType::Success);

        try {
            $this->user->login(
                $values->email,
                $values->password,
            );
        } catch (AuthenticationException $e) {
            Debugger::log($e->getMessage(), ILogger::EXCEPTION);
            $flashMessage = new FlashMessage(self::LoginFail, FlashMessageType::Danger);
        }

        $this->flashMessage($flashMessage);
        $this->redirect(self::RedirectLink);
    }
}
