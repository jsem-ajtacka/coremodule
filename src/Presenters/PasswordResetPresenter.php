<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Presenters;

use JaAdmin\CoreModule\Utils\Email;
use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use Exception;
use JaAdmin\CoreModule\Services\UserService;
use Nette\Application\UI\Form;
use Nette\DI\Attributes\Inject;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Tracy\ILogger;

final class PasswordResetPresenter extends UnsecurePresenter
{
    private const PasswordResetSuccess = "coreModule.passwordReset.flashMessage.passwordResetSuccess";
    private const PasswordResetFail = "coreModule.passwordReset.flashMessage.passwordResetFail";
    private const RedirectLink = ":Core:Login:default";

    #[Inject]
    public UserService $userService;

    #[Inject]
    public Email $email;

    private string $hash;

    public function startup()
    {
        parent::startup();
    }

    public function actionDefault(string $hash)
    {
        $this->hash = $hash;

        if (!$this->userService->isPasswordResetRequestValid($hash)) {
            $this->redirect(self::RedirectLink);
        }
    }

    public function createComponentPasswordResetForm(): Form
    {
        $form = new Form();

        $form->setTranslator($this->translator);

        $form->addPassword("newPassword", "coreModule.passwordReset.form.newPassword.label")
            ->setHtmlAttribute("placeholder", "coreModule.passwordReset.form.newPassword.placeholder")
            ->setRequired();

        $form->addPassword("newPasswordAgain", "coreModule.passwordReset.form.newPasswordAgain.label")
            ->setHtmlAttribute("placeholder", "")
            ->addRule($form::Equal, "coreModule.passwordReset.form.newPasswordAgain.validations.equal", $form['newPassword'])
            ->setRequired();

        $form->addSubmit("submit", "coreModule.passwordReset.form.submit.label");

        $form->onSuccess[] = [$this, "onPasswordResetFormSuccess"];

        return $form;
    }

    public function onPasswordResetFormSuccess(Form $form, ArrayHash $values): void
    {
        $flashMessage = new FlashMessage(self::PasswordResetSuccess, FlashMessageType::Success);

        try {
            $this->userService->changePassword($this->hash, $values->newPassword);
        } catch (Exception $e) {
            Debugger::log($e->getMessage(), ILogger::CRITICAL);
            $flashMessage = new FlashMessage(self::PasswordResetFail, FlashMessageType::Danger);
        }

        $this->flashMessage($flashMessage);
        $this->redirect(self::RedirectLink);
    }
}
