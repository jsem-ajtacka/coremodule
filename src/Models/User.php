<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Nette\Security\IIdentity;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "user")]
class User extends BaseEntity implements IIdentity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    protected int $id;

    #[ORM\Column(type: "string")]
    protected string $password;

    #[ORM\Column(type: "string")]
    protected string $firstname;

    #[ORM\Column(type: "string")]
    protected string $lastname;

    #[ORM\Column(type: "string")]
    protected string $email;

    #[ORM\ManyToMany(targetEntity: "UserRole", inversedBy: "users", cascade: ["persist", "remove"])]
    #[ORM\JoinTable(name: "users_roles")]
    #[ORM\JoinColumn(name: "user_id", referencedColumnName: "id")]
    #[ORM\InverseJoinColumn(name: "role_id", referencedColumnName: "id")]
    private Collection $roles;

    #[Pure]
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getLastname() : string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;
        return $this;
    }

    public function getName(): string
    {
        return $this->firstname . " " . $this->lastname;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function addRole(UserRole $role): self
    {
        $this->roles->add($role);
        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles->map(function ($role) {
            return $role->getTitle();
        })->toArray();
    }

    public function hasRole(string $roleTitle): bool
    {
        return in_array($roleTitle, $this->getRoles());
    }
}
