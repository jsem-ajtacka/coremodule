<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Models;

use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "password_reset_request")]
class PasswordResetRequest extends BaseEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    protected int $id;

    #[ORM\Column(type: "string")]
    protected string $hash;

    #[ORM\Column(type: "datetime")]
    protected DateTime $generatedAt;

    #[ORM\ManyToOne(targetEntity: "User")]
    #[ORM\JoinColumn(name: "user_id", referencedColumnName: "id")]
    private User $user;

    public function getId(): int
    {
        return $this->id;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): self
    {
        $this->hash = $hash;
        return $this;
    }

    public function getGeneratedAt(): DateTime
    {
        return $this->generatedAt;
    }

    public function setGeneratedAt(DateTime $generatedAt): self
    {
        $this->generatedAt = $generatedAt;
        return $this;
    }

    public function isOutdated() : bool
    {
        $validFrom = $this->getGeneratedAt();
        $validTo = ($validFrom)->modify("+1 hour"); // the password reset request is valid only 1 hour
        $now = new DateTime();

        return $validTo->getTimestamp() < $now->getTimestamp();
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }
}
