<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Models;

use Doctrine\ORM\Mapping as ORM;

#[ORM\MappedSuperclass]
class BaseEntity
{
    public function getColumn($column)
    {
        return $this->{$column};
    }
}
