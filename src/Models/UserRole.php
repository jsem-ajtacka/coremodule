<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "user_role")]
class UserRole extends BaseEntity
{
    #[ORM\Id, ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    protected int $id;

    #[ORM\Column(type: "string")]
    protected string $title;

    #[ORM\ManyToMany(targetEntity: "User", mappedBy: "roles", cascade: ["persist", "remove"])]
    private Collection $users;

    #[Pure]
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getRoleId(): string
    {
        return (string)$this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }
}
