<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Models;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "permission")]
class Permission extends BaseEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: "UserRole")]
    #[ORM\JoinColumn(name: "user_role_id", referencedColumnName: "id")]
    private UserRole $userRole;

    #[ORM\ManyToOne(targetEntity: "Extension")]
    #[ORM\JoinColumn(name: "extension_id", referencedColumnName: "id")]
    private Extension $extension;

    #[ORM\Column(type: "privilegeEnum")]
    protected string $privilege;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserRole(): UserRole
    {
        return $this->userRole;
    }

    public function setUserRole(UserRole $userRole): self
    {
        $this->userRole = $userRole;
        return $this;
    }

    public function getExtension(): Extension
    {
        return $this->extension;
    }

    public function setExtension(Extension $extension): self
    {
        $this->extension = $extension;
        return $this;
    }

    public function getPrivilege(): string
    {
        return $this->privilege;
    }

    public function setPrivilege(string $privilege): self
    {
        $this->privilege = $privilege;
        return $this;
    }
}
