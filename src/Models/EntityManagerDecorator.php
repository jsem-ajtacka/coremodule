<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Models;

use Doctrine\ORM\EntityManagerInterface;

final class EntityManagerDecorator extends \Nettrine\ORM\EntityManagerDecorator
{
    public function __construct(EntityManagerInterface $wrapped)
    {
        parent::__construct($wrapped);
        $emConfig = $this->getConfiguration();
        $emConfig->addCustomDatetimeFunction("YEAR", "DoctrineExtensions\Query\Mysql\Year");
        $emConfig->addCustomDatetimeFunction("MONTH", "DoctrineExtensions\Query\Mysql\Month");
        $emConfig->addCustomDatetimeFunction("DAY", "DoctrineExtensions\Query\Mysql\Day");

        $this->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping("enum", "string");
    }
}
