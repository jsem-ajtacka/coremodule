<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Models;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "extension")]
class Extension extends BaseEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    protected int $id;

    #[ORM\Column(type: "string")]
    protected string $key;

    #[ORM\Column(type: "string")]
    protected string $icon;

    #[ORM\Column(type: "boolean")]
    protected bool $active;

    public function getId(): int
    {
        return $this->id;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;
        return $this;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    public function getFirstUpperKey() : string
    {
        return Strings::firstUpper($this->key);
    }

    public function getPresenter(): string
    {
        return ":" . $this->getFirstUpperKey() . ":Overview:default";
    }
}
