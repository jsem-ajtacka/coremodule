<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Utils;

use Nette\Utils\DateTime;

class FlashMessage extends \stdClass
{
    public string $type;
	public DateTime $date;
	public string $text;

	public function __construct(string $text, string $type = null, DateTime $date = null)
	{
		$this->text = $text;
        $this->type = is_null($type) ? FlashMessageType::Info : $type;
		$this->date = is_null($date) ? new DateTime() : $date;
	}

    public function isSuccess() : bool
    {
        return $this->type === FlashMessageType::Success;
    }

    public function isDanger() : bool
    {
        return $this->type === FlashMessageType::Danger;
    }

    public function getIcon() : string
    {
        return match ($this->type) {
            FlashMessageType::Success => "fa fa-check-circle text-success",
            FlashMessageType::Warning => "fa fa-exclamation-circle text-warning",
            FlashMessageType::Danger => "fa fa-triangle-exclamation text-danger",
            default => "fa fa-info-circle",
        };
    }

    public function getStyleClasses() : string
    {
        return match ($this->type) {
            FlashMessageType::Success => "text-success",
            FlashMessageType::Warning => "text-warning",
            FlashMessageType::Danger => "text-danger",
            default => "text-black",
        };
    }
}