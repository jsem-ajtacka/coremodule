<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Utils;

final class KW
{
    public const Entity = "e";
    public const Dot = ".";
    public const Id = "id";

    public const Active = "active";
    public const Date = "date";
    public const Done = "done";
    public const Email = "email";
    public const Hash = "hash";
    public const Key = "key";
    public const Slug = "slug";
    public const Title = "title";
}