<?php

namespace JaAdmin\CoreModule\Utils;

use JaAdmin\CoreModule\Models\EnumType;

final class Privilege extends EnumType
{
    public const View = "view";
    public const Add = "add";
    public const Edit = "edit";
    public const Delete = "delete";

    private const NAME = "privilegeEnum";
    private const VALUES = [self::View, self::Add, self::Edit, self::Delete];

    protected string $name = self::NAME;
    protected array $values = self::VALUES;

    public static function getValue(int $key) : string
    {
        $index = array_search($key, self::VALUES);
        return self::VALUES[$index] ?? self::View;
    }
}