<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Utils;

use Nette\InvalidStateException;
use Nette\Mail\Mailer;
use Nette\Bridges\ApplicationLatte\LatteFactory;
use Nette\Mail\Message;
use Nette\SmartObject;

class Email
{
    use SmartObject;

    private const Exception = 'Parameter %s is not set.';

    private string $from;

    private string $to;

    private string $subject;

    private string $latteTemplate;

    private array $params;

    private Message $email;

    public function __construct(private Mailer $mailer, private LatteFactory $latteFactory)
    {
        $this->email = new Message();
    }

    public function setFrom(string $from): self
    {
        $this->from = $from;
        return $this;
    }

    public function setTo(string $to): self
    {
        $this->to = $to;
        return $this;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;
        return $this;
    }

    public function setLatteTemplate(string $latteTemplate): self
    {
        $this->latteTemplate = $latteTemplate;
        return $this;
    }

    public function setParams(array $params): self
    {
        $this->params = $params;
        return $this;
    }

    private function create(): void
    {
        $latte = $this->latteFactory->create();
        $htmlBody = $latte->renderToString($this->latteTemplate, $this->params);
        $this->email->setFrom($this->from)
            ->addTo($this->to)
            ->setSubject($this->subject)
            ->setHtmlBody($htmlBody);
    }

    public function send(): void
    {
        $this->create();

        if (empty($this->from)) {
            throw new InvalidStateException(sprintf(self::Exception, "from"));
        }

        if (empty($this->to)) {
            throw new InvalidStateException(sprintf(self::Exception, "to"));
        }

        if (empty($this->subject)) {
            throw new InvalidStateException(sprintf(self::Exception, "subject"));
        }

        if (empty($this->latteTemplate)) {
            throw new InvalidStateException(sprintf(self::Exception, "latteTemplate"));
        }

        $this->mailer->send($this->email);
    }
}
