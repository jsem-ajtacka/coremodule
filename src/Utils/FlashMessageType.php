<?php

declare(strict_types=1);

namespace JaAdmin\CoreModule\Utils;

class FlashMessageType extends \stdClass
{
    public const Success = "success";
    public const Info = "info";
    public const Warning = "warning";
    public const Danger = "danger";
}